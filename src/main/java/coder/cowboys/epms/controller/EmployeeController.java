package coder.cowboys.epms.controller;

import coder.cowboys.epms.model.Employee;
import coder.cowboys.epms.service.EmployeeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins="*")
public class EmployeeController {

    @Autowired
    private EmployeeService service;

    @Operation(summary = "Get a employee by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the employee",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = Employee.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid employeeId supplied", content = @Content),
            @ApiResponse(responseCode = "404", description = "Employee not found", content = @Content)}) // @formatter:on
    @GetMapping("/employee/{id}")
    public ResponseEntity<Employee> getEmployeeById(@PathVariable final long id) {
       return ResponseEntity.ok(service.getEmployeeById(id));
    }

    @Operation(summary = "Get all employees")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the employee",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = Employee.class))})})
    @GetMapping("/employees")
    public ResponseEntity<List<Employee>> getEmployees() {
        return ResponseEntity.ok(service.getAllEmployees());
    }


    @Operation(summary = "Add a employee")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Add the employee",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = Employee.class))})}) // @formatter:on
    @PostMapping("/employee")
    public ResponseEntity<Employee> addEmployee(@RequestBody final Employee employee) {
        return ResponseEntity.ok(service.addEmployee(employee));
    }

}
