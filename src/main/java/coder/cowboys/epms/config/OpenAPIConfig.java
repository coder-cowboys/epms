package coder.cowboys.epms.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenAPIConfig {

    @Bean
    public OpenAPI epmsOpenApi() {

        return new OpenAPI()
                .info(new Info().title("EPMS API")
                        .description("Employee Performance Management System")
                        .version("v0.0.1"));
    }

}
