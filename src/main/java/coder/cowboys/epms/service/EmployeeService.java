package coder.cowboys.epms.service;

import coder.cowboys.epms.model.Employee;

import java.util.List;

public interface EmployeeService {

    Employee getEmployeeById(Long employeeId);

    Employee addEmployee(Employee employee);

    List<Employee> getAllEmployees();
}
