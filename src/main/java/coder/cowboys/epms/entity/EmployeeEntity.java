package coder.cowboys.epms.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class EmployeeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "employee_id", nullable = false)

    private Long employeeId;
    @Column(name = "employee_name", nullable = false)
    private String employeeName;

    @Column(name = "art_unit", nullable = false)
    private String artUnit;

}
