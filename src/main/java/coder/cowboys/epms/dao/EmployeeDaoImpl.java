package coder.cowboys.epms.dao;

import coder.cowboys.epms.entity.EmployeeEntity;
import coder.cowboys.epms.model.Employee;
import coder.cowboys.epms.repository.EmployeeRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@Service
public class EmployeeDaoImpl implements EmployeeDao {

    private final EmployeeRepository employeeRepository;

    @Autowired
    private ModelMapper modelMapper;

    public EmployeeDaoImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }


    @Override
    public Employee getEmployeeById(Long employeeId) {
        Optional<EmployeeEntity> employeeEntity = employeeRepository.findById(employeeId);
        return employeeEntity.map(entity -> modelMapper.map(entity, Employee.class)).orElse(null);
    }

    @Override
    public Employee addEmployee(Employee employee) {
        EmployeeEntity entity = modelMapper.map(employee,EmployeeEntity.class);
        return modelMapper.map(employeeRepository.save(entity),Employee.class);
    }

    @Override
    public List<Employee> getAllEmployees() {
        Iterable<EmployeeEntity> employeeEntities = employeeRepository.findAll();
        List<Employee> employeeList = new ArrayList<>();
        for(EmployeeEntity employeeEntity: employeeEntities){
            employeeList.add(modelMapper.map(employeeEntity,Employee.class));
        }
        return employeeList;
    }
}
