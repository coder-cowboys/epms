package coder.cowboys.epms.dao;

import coder.cowboys.epms.model.Employee;

import java.util.List;

public interface EmployeeDao {

    Employee getEmployeeById(Long employeeId);

    Employee addEmployee(Employee employee);

    List<Employee> getAllEmployees();
}
